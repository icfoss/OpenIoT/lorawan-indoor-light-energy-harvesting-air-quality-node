#include <Arduino.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <Wire.h>
#include <LowPower.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"

#define BME_SCK A4
#define BME_MISO A5
#define BME_MOSI 8
#define BME_CS 7

#define SEALEVELPRESSURE_HPA (1013.25)
#define BATTERY_THRESHOLD 2.20

Adafruit_BME680 bme; // I2C
//Adafruit_BME680 bme(BME_CS); // hardware SPI
//Adafruit_BME680 bme(BME_CS, BME_MOSI, BME_MISO,  BME_SCK);


static const PROGMEM u1_t NWKSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
static const u1_t PROGMEM APPSKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
static const u4_t DEVADDR = 0x00000000;


// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

// show debug statements; comment next line to disable debug statements
//#define DEBUG

// use low power sleep; comment next line to not use low power sleep
#define SLEEP

static osjob_t sendjob;
bool next_transmission;
bool first_transmission;


struct {
  short int Temperature;
  long Pressure;
  short int Humidity;
  long Gas; 
//  short int Altitude;  
//  short int IAQ_score ;
  short int Volt;
  char error;
} mydata;

bool sendTransmission = 0;
unsigned long previousMillis = 0;        // will store last time LED was updated

// constants won't change:
const long interval = 1000;             // interval at which to blink (milliseconds)

unsigned int SLEEP_INTERVAL = 21600;                       //* Multiples of 8 in Seconds


const lmic_pinmap lmic_pins = {
  .nss = 6,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 5,
  .dio = {2, 3, 4},
};

void onEvent (ev_t ev) {

  switch (ev) {
    case EV_TXCOMPLETE:
    #ifndef SLEEP
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
    #else
      next_transmission = true;          
    #endif
    break;
  }

}

void do_send(osjob_t* j) {
  //Serial.flush();
 // Serial.println("OnDoSend"); 

  
  if (LMIC.opmode & OP_TXRXPEND)
    {
//      #ifdef DEBUG
//      Serial.println(F("OP_TXRXPEND, not sending"));
//      #endif
    }
  else
    {
      LMIC_setTxData2(1, (unsigned char *)&mydata, sizeof(mydata) - 1, 0);
//      #ifdef DEBUG
//      Serial.println(F(" Packet Sent"));
//      #endif
    }
          

}



void ReadSensor()
{ 
    for (int i=0;i<10;i++){
    
    if (! bme.performReading()) {
   // Serial.println("Failed to perform reading :(");
    return;}
  }
  mydata.Temperature = bme.temperature*100;
  mydata.Pressure = bme.pressure;
  mydata.Humidity = bme.humidity*100;
  mydata.Gas = bme.gas_resistance;

//Serial.println(mydata.Gas);
//  digitalWrite(7,LOW);
//  Serial.flush();
//  mydata.Altitude = bme.readAltitude(SEALEVELPRESSURE_HPA)*100;

  }






void Volt()
{
  double analogvalue = 0; 
  double bat_temp = 0;
  double avg = 0;

  for (byte  i = 0; i < 10; i++)
  {
  analogvalue = analogRead(A0)*4; //3/4 Multiplication Factor
  bat_temp += ((analogvalue * 3.3) / 1024); //ADC voltage*Ref. Voltage/1024
  }
  avg = (bat_temp / 10) + 0.05;
  mydata.Volt = avg * 100;
  //  Serial.flush();
  //  Serial.println(avg); 

  if(avg  >= BATTERY_THRESHOLD){
    //os_runloop_once();
    next_transmission = false;
    do_send(&sendjob);
    
  }
  else{ next_transmission = true; }
  //first_transmission = false;
 
}





void setup() {
  
//  Serial.begin(9600);
  Wire.begin();

  pinMode(7,OUTPUT);


  
  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
  #ifdef PROGMEM
  // On AVR, these values are stored in flash and only copied to RAM
  // once. Copy them to a temporary buffer here, LMIC_setSession will
  // copy them into a buffer of its own again.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
  #else
  // If not running an AVR with PROGMEM, just use the arrays directly
  LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
  #endif

  #if defined(CFG_eu868)
  // Set up the channels
  //used by the Things Network, which corresponds
  // to the defaults of most gateways. Without this, only three base
  // channels from the LoRaWAN specification are used, which certainly
  // works, so it is good for debugging, but can overload those
  // frequencies, so be sure to configure the full frequency range of
  // your network here (unless your network autoconfigures them).
  // Setting up channels should happen after LMIC_setSession, as that
  // configures the minimal channel set.
  // NA-US channels 0-71 are configured automatically

  LMIC_setupChannel(0, 865062500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(1, 865402500, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
  LMIC_setupChannel(2, 865985000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band

  // TTN defines an additional channel at 869.525Mhz using SF9 for class B
  // devices' ping slots. LMIC does not have an easy way to define set this
  // frequency and support for class B is spotty and untested, so this
  // frequency is not configured here.
  #elif defined(CFG_us915)
  // NA-US channels 0-71 are configured automatically
  // but only one group of 8 should (a subband) should be active
  // TTN recommends the second sub band, 1 in a zero based count.
  // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
  LMIC_selectSubBand(1);
  #endif

  // Disable link check validation
  LMIC_setLinkCheckMode(0);

  // TTN uses SF9 for its RX2 window.
  // LMIC.dn2Dr = DR_SF10;

  // Set data rate and tx_ready power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7, 14);
  
  // Start job
  //first_transmission = true;
  
  digitalWrite(7,HIGH);

  bme.begin();
//
   // Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320,500); // 320*C for 150 ms
   // Serial.println("OnSetup"); 
    digitalWrite(7,HIGH);
  delay(100); 

  ReadSensor();
  Volt();
  do_send(&sendjob);

}




void loop() {
  

int sleepcycles = SLEEP_INTERVAL / 8;  // calculate the number of sleepcycles (8s) given the TX_INTERVAL
  extern volatile unsigned long timer0_overflow_count;
  unsigned long currentMillis = millis();
  
  if (currentMillis - previousMillis >= interval)
  {
    previousMillis = currentMillis;
  }
  os_runloop_once();

  #ifndef SLEEP
  os_runloop_once();
  #else



  if (next_transmission == false) {
    os_runloop_once();
  }
    
  else {
      digitalWrite(7,LOW);
   // Serial.println("OnSleep"); 
   // Serial.flush();

    for (int i = 0; i < sleepcycles; i++)
    {
      // Enter power down state for 8 s with ADC and BOD module disabled
      LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
      //LowPower.idle(SLEEP_8S, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_OFF, SPI_OFF, USART0_OFF, TWI_OFF);
      // LMIC uses micros() to keep track of the duty cycle, so
      // hack timer0_overflow for a rude adjustment:
      cli();
      timer0_overflow_count += 8 * 64 * clockCyclesPerMicrosecond();
      sei();
    }
    
      digitalWrite(7,HIGH);
      delay(100);
 
      ReadSensor();
      Volt();
      

  }

#endif
  }
