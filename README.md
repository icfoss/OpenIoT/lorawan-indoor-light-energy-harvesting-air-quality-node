### LoRaWAN Indoor Light Energy Harvesting - Air Quality Node
Harvesting energy plays an important role in increasing the efficiency and lifetime of IoT devices. Energy harvesting systems have some limitations such as unavailability of the energy source from which energy is supposed to be harvested, low amount of harvested energy, inefficiency of the harvesting system, etc. To overcome these limitations, some efforts have been done and new models for harvesting energy have been formed which are discussed in this review concerning the energy source of the harvest. Indoor Photo-voltaic Cell is used to harvest energy from indoor lighting. The harvested energy is stored into a Li-ion battery which is able to supply required amount of power to LoRaWAN Air Quality node based on BME680.

### Prerequisites
1. ULPLoRa (https://gitlab.com/icfoss/OpenIoT/ulplora)
2. AM1815 (PV Cell - 5.0V)
3. Li-Ion Battery including charging circuit
4. BME680
